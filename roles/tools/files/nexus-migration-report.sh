#!/bin/bash

# Declare variables
ROW_FORMAT="%60.60s | %-60.60s | %-30.30s\n"
ENGAGE_REPO="nexus.engageska-portugal.pt"
TELESCOPE_REPO="artefact.skatelescope.org"
CAR_REPO="artefact.skao.int"
NAMESPACE="--all-namespaces"
declare -a NON_CAR_IMAGE_PODS
declare -a CAR_IMAGE_PODS

# Check arguments for request for help, show namespaces as well
# as possible arguments
for arg in $@ ; do
    if [[ $arg == "--help" ]] || [[ $arg == "-h" ]] || [[ $# -gt 1 ]] ; then
        printf "Usage: nexus-migration-report [namespace]\n\n"
        printf "Current namespaces:\n"
        kubectl get namespaces
        exit 0
    fi
done

# Set the namespace to report on if set
if [[ -n $1 ]] ; then
    NAMESPACE="--namespace $1"
fi

# Parse all the running pods and print their images and repos 
printf "${ROW_FORMAT}" "Pod Name" "Image" "Repository" 
while read -r LINE ; do
    FIELDS=(${LINE})
    POD=${FIELDS[0]}
    REPO=$(cut -d "/" -f 1 <<< ${FIELDS[1]})
    IMAGE=$(cut -d "/" -f 2- <<< ${FIELDS[1]})
    
    if [[ "${REPO}" == "${ENGAGE_REPO}" ]] || [[ "${REPO}" == "${TELESCOPE_REPO}" ]] ; then
        NON_CAR_IMAGE_PODS+=("${POD}")
        printf "${ROW_FORMAT}" "${POD}" "${IMAGE}" "${REPO}"
    elif [[ "${REPO}" == "${CAR_REPO}" ]] ; then
        CAR_IMAGE_PODS+=("${POD}")
        printf "${ROW_FORMAT}" "${POD}" "${IMAGE}" "${REPO}"
    fi
done < <(kubectl get pods ${NAMESPACE} -o=jsonpath='{range .items[*]}{"\n"}{.metadata.name}{" "}{range .spec.containers[*]}{.image}{end}{end}' | sort | uniq)

# Post Processing
NUM_PODS=$((${#NON_CAR_IMAGE_PODS[@]} + ${#CAR_IMAGE_PODS[@]}))
NUM_CAR_PODS=${#CAR_IMAGE_PODS[@]}

if [[ $NUM_PODS -eq 0 ]] ; then
    MIGRATION_PERC=0
else
    MIGRATION_PERC=$(($NUM_CAR_PODS * 100 / $NUM_PODS))
fi

printf "\nMigration Progress: %d/%d (%d%%)\n" "$NUM_CAR_PODS" "$NUM_PODS" "$MIGRATION_PERC"
