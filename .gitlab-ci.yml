# GitLab CI in conjunction with GitLab Runner can use Docker Engine to test and build any application.
# Docker, when used with GitLab CI, runs each job in a separate and isolated container using the predefined image that is set up in .gitlab-ci.yml.
# In this case we use the latest python docker image to build and test this project.
# image: $SKA_PYTHON_PYTANGO_BUILDER_IMAGE
image: docker:19

variables:
  DOCKER_DRIVER: overlay2

# cache is used to specify a list of files and directories which should be cached between jobs. You can only use paths that are within the project workspace.
# If cache is defined outside the scope of jobs, it means it is set globally and all jobs will use that definition
cache:
  paths:
    - build
    - reports
# before_script is used to define the command that should be run before all jobs, including deploy jobs, but after the restoration of artifacts.
# This can be an array or a multi-line string.

stages:
  - linting
  - test
  - deploy

# The YAML file defines a set of jobs with constraints stating when they should be run.
# You can specify an unlimited number of jobs which are defined as top-level elements with an arbitrary name and always
#  have to contain at least the script clause.
# In this case we have only the test job which produces a coverage report and the unittest output (see setup.cfg), and
#  the coverage xml report is moved to the reports directory while the html output is persisted for use by the pages
#  job. TODO: possibly a candidate for refactor / renaming later on.
# test:
#   stage: test
# #  tags:
# #   - k8srunner
#   script:
#   #  - pipenv run python setup.py test
#    - python3 setup.py test
#    - mv coverage.xml ./build/reports/code-coverage.xml
#   artifacts:
#     paths:
#     - ./build
#     - htmlcov

lint:
  stage: linting
  image: python:3.8
  tags:
    - k8srunner
  script:
    - python3 -m pip install ansible==4.6.0 ansible-core==2.11.5 ansible-lint==5.3.2 flake8 yamllint  pytest-cov junit_xml coverage==5.5
    - echo $GIT_BASE
    - echo $CI_JOB_TOKEN
    - echo ${GIT_BASE:-"https://gitlab-ci-token:$CI_JOB_TOKEN@gitlab.com/ska-telescope"}
    - make lint GIT_BASE=${GIT_BASE:-"https://gitlab-ci-token:$CI_JOB_TOKEN@gitlab.com/ska-telescope"}
    - make lint
  artifacts:
    when: always
    paths:
      - build

test:
  stage: test
  image: docker:19 # docker:19 is based on alpine linux 3.13 with python 3.8
  tags:
    - k8srunner
  allow_failure: false
  variables:
    KUBERNETES_CPU_REQUEST: 3000m
    KUBERNETES_MEMORY_REQUEST: 2Gi
    KUBERNETES_MEMORY_LIMIT: 4Gi
    KUBERNETES_EPHEMERAL_STORAGE_REQUEST: 1Gi
    KUBERNETES_EPHEMERAL_STORAGE_LIMIT: 5Gi

    KUBERNETES_HELPER_CPU_REQUEST: 3000m
    KUBERNETES_HELPER_MEMORY_REQUEST: 2Gi
    KUBERNETES_HELPER_MEMORY_LIMIT: 4Gi
    KUBERNETES_HELPER_EPHEMERAL_STORAGE_REQUEST: 1Gi
    KUBERNETES_HELPER_EPHEMERAL_STORAGE_LIMIT: 5Gi

    KUBERNETES_SERVICE_CPU_REQUEST: 3000m
    KUBERNETES_SERVICE_MEMORY_REQUEST: 2Gi
    KUBERNETES_SERVICE_MEMORY_LIMIT: 4Gi
    KUBERNETES_SERVICE_EPHEMERAL_STORAGE_REQUEST: 1Gi
    KUBERNETES_SERVICE_EPHEMERAL_STORAGE_LIMIT: 5Gi

  script:
    - docker info
    - apk update && apk add --no-cache
      python3-dev py3-pip gcc git curl build-base
      autoconf automake py3-cryptography linux-headers
      musl-dev libffi-dev openssl-dev openssh bash
      rsync openssh-client
    - python3 --version
    - pip install --upgrade pip
    - python3 -m pip install ansible==4.6.0 ansible-core==2.11.5 ansible-lint==5.3.2 flake8 yamllint molecule==3.4.0 molecule-docker==0.3.4 pytest-molecule pytest-cov junit_xml coverage==5.5 jmespath
    - ansible --version
    - molecule --version
    - cp roles/ssh/files/project-servers-list.json /tmp/project-servers-list.json
    - echo "CI_JOB_TOKEN=$CI_JOB_TOKEN"
    - echo "MY_JOB_TOKEN=$MY_JOB_TOKEN"
    - make test
  artifacts:
    when: always
    paths:
      - build
    reports:
      junit:
        - build/reports/unit-tests.xml
        - build/reports/verify-*.xml
      coverage_report:
        coverage_format: cobertura
        path: build/reports/code-coverage.xml

list_dependencies:
  stage: test
  image: artefact.skao.int/ska-tango-images-pytango-builder:9.3.10
  before_script:
    - python3 -m pip install -r requirements.txt
    - pip3 install pipdeptree

  script:
    # - pipenv graph >> pipenv_deps.txt
    - pipdeptree --json >> pip_deps.json
    - pipdeptree >> pip_deps.txt
    - dpkg -l >> system_deps.txt
    - awk 'FNR>5 {print $2 ", " $3}' system_deps.txt >> system_deps.csv
    - mkdir .public
    - cp pip_deps.txt .public/
    - cp pip_deps.json .public/
    - cp system_deps.txt .public/
    - cp system_deps.csv .public/
    - mv .public public
  artifacts:
    paths:
      - public

# update:
#   stage: deploy
#   tags:
#     - k8srunner
#   script:
#     - make bifrost
#   only:
#     - master

pages:
  stage: deploy
  image: artefact.skao.int/ska-tango-images-pytango-builder:9.3.10
  before_script:
    - python3 -m pip install -r requirements.txt
  tags:
    - k8srunner
  dependencies:
    - test
  script:
    - ls -la
    - mkdir .public
    - cp -r build/reports/htmlcov/* .public
    - rm -rf build/reports/htmlcov
    - mv .public public
  artifacts:
    paths:
      - public
    expire_in: 30 days

# Standardised included jobs
# Uncomment the variables and helm chart job if you build k8s resources in your project
# variables:
#   CHARTS_TO_PUBLISH: chart-1 chart-2
include:
  # Helm Chart Publish
  # https://developer.skatelescope.org/en/latest/development/software_package_release_procedure.html#package-and-publish-helm-charts-to-the-ska-helm-chart-repository
  # - project: 'ska-telescope/templates-repository'
  #   file: 'gitlab-ci/includes/helm_publish.yml'

  # Create Gitlab CI badges from CI metrics
  # https://developer.skatelescope.org/en/latest/tools/continuousintegration.html#automated-collection-of-ci-health-metrics-as-part-of-the-ci-pipeline
  - project: "ska-telescope/templates-repository"
    file: "gitlab-ci/includes/post_step.yml"
