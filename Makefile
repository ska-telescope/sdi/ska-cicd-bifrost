# Set dir of Makefile to a variable to use later
MAKEPATH := $(abspath $(lastword $(MAKEFILE_LIST)))
BASEDIR := $(notdir $(patsubst %/,%,$(dir $(MAKEPATH))))

# find IP addresses of this machine, setting THIS_HOST to the first address found
THIS_HOST := $(shell (ip a 2> /dev/null || ifconfig) | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p' | head -n1)
SSH_USER ?= ubuntu
BIFROST_IP ?= 130.246.214.237
PRIVATE_VARS ?= dev_bifrost_vars.yml
INVENTORY_FILE ?= ./inventory_bifrost
CLUSTER_KEYPAIR ?= someones-key-pair
COLLECTIONS_PATHS ?= ./collections
ANSIBLE_COLLECTIONS_PATHS := $(COLLECTIONS_PATHS):~/.ansible/collections:/usr/share/ansible/collections
STACK_CLUSTER_PLAYBOOKS := $(COLLECTIONS_PATHS)/ansible_collections/ska_cicd/stack_cluster/playbooks
DOCKER_PLAYBOOKS := $(COLLECTIONS_PATHS)/ansible_collections/ska_cicd/docker_base/playbooks
SSH_KEY ?= ~/.ssh/id_rsa
STAGE ?= test
CI_JOB_TOKEN ?=
SKIP_TAGS ?=
RUN_TAGS ?=
V ?=
EXTRA_VARS ?= "mode=all"
CLUSTER_INVENTORY ?= ./ska-cicd-distribute-ssh-keys/inventory_ssh_keys
MARVIN_SLACK_TOKEN ?=
# GIT_BASE=https://gitlab-ci-token:$(CI_JOB_TOKEN)@gitlab.com/ska-telescope# PrivateRules.mak value in case you use rtest
GIT_BASE ?= git@gitlab.com:ska-telescope# Set this to GIT_BASE=https://gitlab-ci-token:$(CI_JOB_TOKEN)@gitlab.com/ska-telescope if you have CI_JOB_TOKEN set.

# Fixed variables
TIMEOUT = 86400

# Docker and Gitlab CI variables
RDEBUG ?= ""
CI_ENVIRONMENT_SLUG ?= development
CI_PIPELINE_ID ?= pipeline$(shell tr -c -d '0123456789abcdefghijklmnopqrstuvwxyz' </dev/urandom | dd bs=8 count=1 2>/dev/null;echo)
CI_JOB_ID ?= job$(shell tr -c -d '0123456789abcdefghijklmnopqrstuvwxyz' </dev/urandom | dd bs=4 count=1 2>/dev/null;echo)
GITLAB_USER ?= ""
CI_BUILD_TOKEN ?= ""
REPOSITORY_TOKEN ?= ""
REGISTRY_TOKEN ?= ""
GITLAB_USER_EMAIL ?= "nobody@example.com"
DOCKER_VOLUMES ?= /var/run/docker.sock:/var/run/docker.sock
CI_APPLICATION_TAG ?= $(shell git rev-parse --verify --short=8 HEAD)
DOCKERFILE ?= Dockerfile
EXECUTOR ?= docker

# Molecule variables
MOLECULE_SCENARIO_NAME ?= default

# define overides for above variables in here
-include PrivateRules.mak

.PHONY: help
.DEFAULT_GOAL := help

vars: ## Display variables - pass in DISPLAY and XAUTHORITY
	@echo "Current variable settings:"
	@echo "PRIVATE_VARS: $(PRIVATE_VARS)"
	@echo "INVENTORY_FILE: $(INVENTORY_FILE)"
	@echo "EXTRA_VARS: $(EXTRA_VARS)"
	@echo "CLUSTER_KEYPAIR: $(CLUSTER_KEYPAIR)"
	@echo "ANSIBLE_COLLECTIONS_PATHS: $(ANSIBLE_COLLECTIONS_PATHS)"
	@echo "STACK_CLUSTER_PLAYBOOKS: $(STACK_CLUSTER_PLAYBOOKS)"
	@echo "THIS_HOST: $(THIS_HOST)"
	@echo "VALUES: $(VALUES)"
	@echo "USER: $(USER)"
	@echo "BIFROST_IP: $(BIFROST_IP)"
	@echo "CI_JOB_TOKEN: $(CI_JOB_TOKEN)"
	@echo "CURDIR: $(CURDIR)"

uninstall:  # uninstall collections
	rm -rf $(COLLECTIONS_PATHS)/ansible_collections/* $(COLLECTIONS_PATHS)/.collected

install:  ## Install dependent ansible collections
	if [ -f $(COLLECTIONS_PATHS)/.collected ]; then \
	echo "Already collected !"; \
	else \
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-galaxy collection install \
	-r tests/molecule/default/requirements.yml -p ./collections; \
	ansible-galaxy install -r tests/molecule/default/requirements.yml --roles-path ./roles; \
	touch $(COLLECTIONS_PATHS)/.collected; \
	fi

getinventory:  ## get the ska-cicd-distribute-ssh-keys inventory
	if [ -n "$${MY_JOB_TOKEN}" ]; then export CI_JOB_TOKEN="$${MY_JOB_TOKEN}"; else export CI_JOB_TOKEN="$(CI_JOB_TOKEN)"; fi; \
	if [ -n "$${CI_JOB_TOKEN}" ]; then GIT_BASE="https://gitlab-ci-token:$${CI_JOB_TOKEN}@gitlab.com/ska-telescope"; else GIT_BASE="$(GIT_BASE)"; fi; \
	echo after GIT_BASE=$${GIT_BASE}; \
	if [ ! -d "ska-cicd-distribute-ssh-keys" ]; then git clone $${GIT_BASE}/sdi/ska-cicd-distribute-ssh-keys.git; fi

reinstall: uninstall install ## reinstall collections

all: build

check_production:
	(grep 'production: true' $(PRIVATE_VARS) && (echo "$(INVENTORY_FILE)" | grep "bifrost") ) || \
	(grep 'production: false' $(PRIVATE_VARS) && (echo "$(INVENTORY_FILE)" | grep -v "bifrost") ) || \
	(printf "aborting, as this is not production ($(PRIVATE_VARS)) \n"; exit 1)

open:
	@if openstack keypair show -f json $(CLUSTER_KEYPAIR) | jq '.name' | grep -q "$(CLUSTER_KEYPAIR)"; then \
		echo "Connected to Openstack with $(CLUSTER_KEYPAIR) keypair set"; \
	else \
		echo "ERROR: $(CLUSTER_KEYPAIR) not recognised. Make sure your connection to Openstack is set up correctly. Did you source your openrc file?"; \
		false; \
	fi

build: install build_node build_common build_docker bifrost ## set up a new bifrost server

build_node: open install ## check_production Build nodes based on heat-cluster
	[ ! -e $(PRIVATE_VARS) ] || touch $(PRIVATE_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook -e @$(PRIVATE_VARS) \
					 --extra-vars="$(EXTRA_VARS)" \
					 --extra-vars="cluster_keypair=$(CLUSTER_KEYPAIR)" \
					 -i $(STACK_CLUSTER_PLAYBOOKS)/inventory \
					 $(STACK_CLUSTER_PLAYBOOKS)/cluster-infra.yml \
	--extra-vars="inventory_file=../../../../$(INVENTORY_FILE)"

build_common: install  ## apply the common roles
	[ ! -e $(PRIVATE_VARS) ] || touch $(PRIVATE_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook  -i $(INVENTORY_FILE) $(STACK_CLUSTER_PLAYBOOKS)/common.yml \
					  -e @$(PRIVATE_VARS) \
					  --extra-vars="$(EXTRA_VARS)" $(V)

build_docker: install  ## apply the docker roles
	[ ! -e $(PRIVATE_VARS) ] || touch $(PRIVATE_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook  -i $(INVENTORY_FILE) $(DOCKER_PLAYBOOKS)/docker.yml \
					  -e @$(PRIVATE_VARS) \
					  --extra-vars="$(EXTRA_VARS)" $(V)

build_bifrost: install getinventory ## apply bifrost roles
	mkdir -p  roles/ssh/files/; \
	openstack server list --name $(CLUSTER_NAME) -f json | jq 'del(.[].ID) | del(.[].Flavor) | del(.[].Status) | del(.[].Image) | . | [. | map(.) | .[] | {host: .Name, ip: .Networks[][0]}]' > /tmp/project-servers-list.json; \
	[ ! -e $(PRIVATE_VARS) ] || touch $(PRIVATE_VARS)
	[ ! -e $(CLUSTER_INVENTORY) ] || touch $(CLUSTER_INVENTORY)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook   -i $(CLUSTER_INVENTORY) -i $(INVENTORY_FILE) setup_bifrost.yml \
					  -e @$(PRIVATE_VARS) \
					  -e "marvin_slack_token=$(MARVIN_SLACK_TOKEN)" \
					  --skip-tags "$(SKIP_TAGS)" \
						--tags "$(RUN_TAGS)" \
					  --extra-vars $(EXTRA_VARS) $(V)

plan: getinventory ## list all tasks that will be evaluated - ignores "mode"
	[ ! -e $(PRIVATE_VARS) ] || touch $(PRIVATE_VARS)
	[ ! -e $(CLUSTER_INVENTORY) ] || touch $(CLUSTER_INVENTORY)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook  -i $(CLUSTER_INVENTORY) -i $(INVENTORY_FILE) setup_bifrost.yml \
					  -e @$(PRIVATE_VARS) \
					  --skip-tags "$(SKIP_TAGS)" \
						--tags "$(RUN_TAGS)" \
					  --extra-vars $(EXTRA_VARS) $(V) \
					  --list-tasks
clean_nodes:
	[ ! -e $(PRIVATE_VARS) ] || touch $(PRIVATE_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook -e @$(PRIVATE_VARS) \
					 --extra-vars="$(EXTRA_VARS)" \
					 -i $(STACK_CLUSTER_PLAYBOOKS)/inventory \
					 $(STACK_CLUSTER_PLAYBOOKS)/remove-cluster-infra.yml

clean: clean_nodes  ## destroy the nodes - CAUTION THIS DELETES EVERYTHING!!!

check_nodes: ##check_production ## Check nodes based on heat-cluster
	ansible -i ./$(INVENTORY_FILE) cluster \
	                         -m shell -a 'df; lsblk'

lint: install ## Lint check playbook
	yamllint -d "{extends: relaxed, rules: {line-length: {max: 256}, indentation: disable}}" \
			setup_bifrost.yml \
			roles/* \
			playbooks/test.yml
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-lint --exclude=tests/molecule/default/requirements.yml \
			setup_bifrost.yml \
			./tests/molecule \
			roles/* \
			playbooks/test.yml
	flake8 ./tests/molecule \
			roles/*


heimdall: connect

bridge_of_rainbows: build

bifrost: build_bifrost

ragnarok:
	make clean_nodes EXTRA_VARS="ireallymeanit=yes"

connect: ## Add SSH Forwarding to local machine and SSH into BIFROST
	@echo "Connecting to the Bifrost as $(SSH_USER)";
	@echo "###########################################";
	#@eval "$$(ssh-agent)" && ssh-add $(SSH_KEY) && echo "Attempting SSH Connection" && echo "#########################" && echo "";
	ssh -A $(SSH_USER)@$(BIFROST_IP);
	@echo "Welcome back"; \

smoketest: ## Smoke test for new created cluster
	ansible-playbook -i $(INVENTORY_FILE) playbooks/test.yml -e @$(PRIVATE_VARS) -vvv

link:
	rm -f tests/molecule/roles
	ln -s ../../roles tests/molecule/roles

test: reinstall link getinventory ## run molecule tests locally outside gitlab-runner
	BASE=$$(pwd); \
	mkdir -p $${BASE}/build/reports; \
	rm -rf $${BASE}/tests/.coverage* $${BASE}/tests/.pytest_cache $${BASE}/build/reports/*; \
	if [ -n "$${MY_JOB_TOKEN}" ]; then export CI_JOB_TOKEN="$${MY_JOB_TOKEN}"; else export CI_JOB_TOKEN="$(CI_JOB_TOKEN)"; fi; \
	export ANSIBLE_CALLBACK_WHITELIST=junit; \
	export JUNIT_TASK_CLASS="True"; \
	export JUNIT_OUTPUT_DIR=$${BASE}/build/reports; \
	export CURDIR=$(CURDIR); \
	cd tests && pytest \
			--junitxml=$${BASE}/build/reports/unit-tests.xml \
			--cov \
			--cov-report term \
			--cov-report html:$${BASE}/build/reports/htmlcov \
			--cov-report xml:$${BASE}/build/reports/code-coverage.xml \
	        --log-file=$${BASE}/build/reports/pytest-logs.txt -v; \
		   RC=$$?; \
		   echo "###### pytest logs #########"; \
		   cat $${BASE}/build/reports/pytest-logs.txt; \
		   echo "RC: $${RC}"; \
           echo "###### code coverage #######"; \
           echo cat $${BASE}/build/reports/code-coverage.xml; \
 		   echo "###### unit tests #######"; \
		   cat $${BASE}/build/reports/unit-tests.xml; \
	exit $${RC}

molecule: reinstall link ## run molecule tests but don't destroy container
	BASE=$$(pwd); \
	export ANSIBLE_ROLES_PATH=$${BASE}/roles;  env | grep ANSIBLE; \
	export CI_JOB_TOKEN=$(CI_JOB_TOKEN); \
	cd tests && MOLECULE_NO_LOG="false" molecule --debug test --destroy=never --scenario-name=$(MOLECULE_SCENARIO_NAME)

verify: ## rerun molecule verify - must run make molecule first
	BASE=$$(pwd); \
	export ANSIBLE_COLLECTIONS_PATHS=$${BASE}/collections \
	       ANSIBLE_COLLECTIONS_PATH=$${BASE}/collections; \
	export ANSIBLE_ROLES_PATH=$${BASE}/roles;  env | grep ANSIBLE; \
	cd tests && MOLECULE_NO_LOG="false" molecule verify --scenario-name=$(MOLECULE_SCENARIO_NAME)

destroy: ## run molecule destroy - cleanup after make molecule
	BASE=$$(pwd); \
	export ANSIBLE_COLLECTIONS_PATHS=$${BASE}/collections \
	       ANSIBLE_COLLECTIONS_PATH=$${BASE}/collections; \
	export ANSIBLE_ROLES_PATH=$${BASE}/roles;  env | grep ANSIBLE; \
	export CI_JOB_TOKEN=$(CI_JOB_TOKEN); \
	cd tests && MOLECULE_NO_LOG="false" molecule destroy

rtest:  ## run make $(STAGE) using gitlab-runner - default: test
	BASE=$$(pwd); \
	if [ -n "$(RDEBUG)" ]; then DEBUG_LEVEL=debug; else DEBUG_LEVEL=warn; fi && \
	gitlab-runner --log-level $${DEBUG_LEVEL} exec $(EXECUTOR) \
    --docker-volumes  $(DOCKER_VOLUMES) \
    --docker-pull-policy always \
	--timeout $(TIMEOUT) \
	--env "GITLAB_USER=$(GITLAB_USER)" \
	--env "REGISTRY_TOKEN=$(REGISTRY_TOKEN)" \
	--env "CI_BUILD_TOKEN=$(CI_BUILD_TOKEN)" \
	--env "CI_JOB_TOKEN=$(CI_JOB_TOKEN)" \
	--env "MY_JOB_TOKEN=$(CI_JOB_TOKEN)" \
	--env "GIT_BASE=$(GIT_BASE)" \
	--env "TRACE=1" \
	--env "DEBUG=1" \
	$(STAGE) || true

ca_setup: ## setup Certificate Authority (CA) on Terminus
	[ ! -e $(PRIVATE_VARS) ] || touch $(PRIVATE_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook  playbooks/ca/setup_ca.yml \
					  -i $(INVENTORY_FILE) \
					  -e @$(PRIVATE_VARS) \
					  -e "$(EXTRA_VARS)" \
					  $(V)

ca_sign: ## sign a certificates on client hosts using the Certificate Authority (CA) on Terminus
	[ ! -e $(PRIVATE_VARS) ] || touch $(PRIVATE_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook  playbooks/ca/sign_certificates.yml \
					  -i $(INVENTORY_FILE) \
					  -i $(CA_TARGET_INVENTORY) \
					  -e @$(PRIVATE_VARS) \
					  -e "$(EXTRA_VARS)" \
					  $(V)

ca_revoke: ## create a certificate revocation list (CRL) for a client certificate and store it on Terminus
	[ ! -e $(PRIVATE_VARS) ] || touch $(PRIVATE_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook  playbooks/ca/generate_crl.yml \
					  -i $(INVENTORY_FILE) \
					  -i $(CA_TARGET_INVENTORY) \
					  -e @$(PRIVATE_VARS) \
					  -e "$(EXTRA_VARS)" \
					  $(V)

ca_delete: ## delete aertificates on client hosts (WARNING: this will delete all certificates on all client hosts)
	[ ! -e $(PRIVATE_VARS) ] || touch $(PRIVATE_VARS)
	[ ! -e $(PRIVATE_VARS) ] || touch $(PRIVATE_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook  playbooks/ca/delete_certificates.yml \
					  -i $(INVENTORY_FILE) \
					  -i $(CA_TARGET_INVENTORY) \
					  -e @$(PRIVATE_VARS) \
					  -e "$(EXTRA_VARS)" \
					  $(V)

help:  ## show this help.
	@echo "make targets:"
	@grep -E '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ": .*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
	@echo ""; echo "make vars (+defaults):"
	@grep -E '^[0-9a-zA-Z_-]+ \?=.*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = " \\?= "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
